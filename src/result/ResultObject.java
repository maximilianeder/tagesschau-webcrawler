package result;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Maximilian Eder
 *
 */
public class ResultObject implements Result{
	private HashMap<String, ArrayList<String>> map;
	private String url;
	private String head;
	private ArrayList<String> links;


	/**
	 * Creates a new ResultObject of a Site
	 * @param url Site URL
	 * @param head head tag or class of the Result
	 * @param map Data of the result, descriptors mapped to a List of info
	 * @param links All the links found in the ResultObject
	 */
	public ResultObject(String url, String head, HashMap<String, ArrayList<String>> map, ArrayList<String> links) {
		this.map = map;
		this.url = url;
		this.head = head;
		this.links = links;
	}

	/**
	 * Returns the data map
	 * @return Data of the result, descriptors mapped to a List of info
	 */
	public HashMap<String, ArrayList<String>> getMap() {
		return map;
	}

	/**
	 * Returns the Site URL
	 * @return Site URL
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Returns the ResultObjects head tag or class
	 * @return ResultObjects head tag or class
	 */
	public String getHead() {
		return head;
	}

	/**
	 * Returns all the Links found in the ResultObject. Null, if addLinks set to false
	 * @return ArrayList of contained links in the ResultObject
	 */
	public ArrayList<String> getLinks() {
		return links;
	}

	public void print() {
		System.out.println("Result of the analysis of " + url + " - head " + head + "\n");
		Set<String> set = map.keySet();
		for (String string : set) {
			ArrayList<String> values = map.get(string);
			if(values == null) continue;

			System.out.println(string + ":");
			for (String value : values) {
				System.out.println(value);
			}
			System.out.println();
		}
	}

	public boolean isRelevant() {
		boolean isValueable = false;
		Set<String> keySet = map.keySet();
		for (String string : keySet) {
			isValueable = isValueable || !map.get(string).isEmpty();
		}
		isValueable |= links != null && !links.isEmpty();
		return isValueable;
	}


	@Override
	public String toJSON() {
		if(!isRelevant()) return "NULL";

		String urlInfo = "\"url\": \"" + url + "\",";
		String headInfo = "\"head\": \"" + head + "\",";
		String mapInfo = "\"map\": [ ";
		for (String key : map.keySet()) {
			String keyInfo = "\"key\": \"" + key + "\",";
			String textInfo = "\"object\": [ ";
			ArrayList<String> info = map.get(key);
			for (String string : info) {
				textInfo += "\"" + modString(string) + "\" ,";
			}
			textInfo = textInfo.substring(0, textInfo.length()-1) + "]";
			mapInfo += "{" + keyInfo + textInfo + "},";
		}
		mapInfo = mapInfo.substring(0, mapInfo.length()-1) + "],";
		String linkInfo = "\"links\": [ ";
		if (links != null) {
			for (String link : links) {
				linkInfo += "\"" + link + "\",";
			} 
		}
		linkInfo = linkInfo.substring(0, linkInfo.length()-1) + "]";
		return "{" + urlInfo + headInfo + mapInfo + linkInfo + "}";
	}

	private String modString(String string) {
		return string.replaceAll("\"", "'");
	}

}
