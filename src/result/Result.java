package result;
import java.util.List;

/**
 * @author Maximilian Eder
 *
 */
public interface Result {

	/**
	 * Method should print out the Result on the Console
	 */
	public void print();

	/**
	 * Determines, whether the Result in question is valuable for further processing
	 * @return Returns true, if the Result is not empty
	 */
	public boolean isRelevant();

	/**
	 * Used to export Results to a usable format (JSON)
	 * @return Returns a string in JSON-format, that stores all of the Results information
	 */
	public String toJSON();

	/**
	 * Converts a List of SiteResults into a JSON-String that can be used for further processing
	 * @param results List of SiteResults
	 * @return String with JSON representation of the SiteResults
	 */
	public static String toJSON(List<SiteResult> results) {
		String head = "\"sites\": [ ";
		for (SiteResult siteResult : results) {
			head += siteResult.toJSON() + ",";
		}
		return "{" + head.substring(0, head.length() - 1) + "]}";
	}
}
