package result;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import file.Read;

public class ResultParser {

	public static ArrayList<SiteResult> siteResultsFromJSON(File jsonInput) {
		ArrayList<SiteResult> list = new ArrayList<>();

		JSONObject jo = new JSONObject(arrayToString(Read.read(jsonInput)));
		JSONArray sites = jo.getJSONArray("sites");
		for (int i = 0; i < sites.length(); i++) {
			JSONObject site = sites.getJSONObject(i);
			String url = site.getString("url");
			JSONArray results = site.getJSONArray("results");
			ArrayList<ResultObject> resultArray = getResults(results);
			SiteResult sr = new SiteResult(url, resultArray);
			list.add(sr);
		}

		return list;
	}

	private static ArrayList<ResultObject> getResults(JSONArray results) {
		ArrayList<ResultObject> resultList = new ArrayList<>();

		for (int i = 0; i < results.length(); i++) {
			JSONObject result = results.getJSONObject(i);

			String url = result.getString("url");
			String head = result.getString("head");
			JSONArray mapArray = result.getJSONArray("map");
			HashMap<String, ArrayList<String>> map = getMap(mapArray);
			JSONArray linksArray = result.getJSONArray("links");
			ArrayList<String> links = getArrayListFromJSONArray(linksArray);

			resultList.add(new ResultObject(url, head, map, links));
		}

		return resultList;
	}

	private static HashMap<String, ArrayList<String>> getMap(JSONArray mapArray) {
		HashMap<String, ArrayList<String>> hm = new HashMap<>();

		for (int i = 0; i < mapArray.length(); i++) {
			JSONObject result = mapArray.getJSONObject(i);

			String key = result.getString("key");

			JSONArray objsArray = result.getJSONArray("object");
			ArrayList<String> objects = getArrayListFromJSONArray(objsArray);

			hm.put(key, objects);
		}

		return hm;
	}

	private static ArrayList<String> getArrayListFromJSONArray(JSONArray linksArray) {
		ArrayList<String> al = new ArrayList<>();
		for (int i = 0; i < linksArray.length(); i++) {
			al.add(linksArray.getString(i));
		}
		return al;
	}

	private static String arrayToString(String[] crawlerCommand) {
		String collection = "";
		for (String string : crawlerCommand) {
			collection += clearUp(string);
		}
		return collection;
	}

	private static String clearUp(String string) {
		String collection = "";
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			if (c != '\t') {
				if (c == '\n') {
					c = ' ';
				}
				collection += c;
			}
		}
		return collection;
	}

}
