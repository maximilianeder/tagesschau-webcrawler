package result;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * @author Maximilian Eder
 *
 */
public class SiteResult implements Result {
	private ArrayList<ResultObject> results;
	private HashMap<String, ArrayList<ResultObject>> headMap;
	private String url;
	private HashSet<String> links;

	/**
	 * Creates a new SiteResult for a URL
	 * @param siteUrl Site URL
	 * @param results List of the {@link ResultObject}s from that site
	 */
	public SiteResult(String siteUrl, ArrayList<ResultObject> results) {
		url = siteUrl;
		links = new HashSet<>();
		this.results = new ArrayList<>();
		headMap = new HashMap<>();
		for (ResultObject result : results) {
			addResult(result);
		}
	}

	/**
	 * Adds a {@link ResultObject} to the SiteResult
	 * @param result {@link ResultObject} to be added
	 */
	public void addResult(ResultObject result) {
		results.add(result);

		ArrayList<ResultObject> headList = headMap.get(result.getHead());
		if (headList == null) {
			headList = new ArrayList<>();
		}
		headList.add(result);
		headMap.put(result.getHead(), headList);

		if (result != null && result.getLinks() != null) {
			links.addAll(result.getLinks());
		}
	}

	/**
	 * Filters all the result by the head of the {@link ResultObject}s and returns a list with all matching {@link ResultObject}s
	 * @param head head-detail of the {@link ResultObject}s that are wished to be obtained
	 * @return All matching {@link ResultObject}s
	 */
	public ArrayList<ResultObject> getResultsByHead(String head) {
		ArrayList<ResultObject> ret = new ArrayList<>();
		for (String key : headMap.keySet()) {
			if (key.contains(head)) {
				ret.addAll(headMap.get(key));
			}
		}
		return ret;
	}

	/**
	 * Returns a List of Results
	 * @return List of Results
	 */
	public ArrayList<ResultObject> getResults() {
		return results;
	}

	/**
	 * Returns a HashSet of all links that were found in one of the specified CrawlerObjects 
	 * @return HashSet of links
	 */
	public HashSet<String> getLinks() {
		return links;
	}

	/**
	 * Returns the Sites URL
	 * @return Site URL
	 */
	public String getUrl() {
		return url;
	}

	public void print() {
		if (!isRelevant())
			return;

		System.out.println("Result of the analysis of " + url + "\n");
		for (ResultObject result : results) {
			result.print();
		}
	}

	@Override
	public boolean isRelevant() {
		return !results.isEmpty();
	}

	@Override
	public String toJSON() {
		String urlInfo = "\"url\": \"" + url + "\", ";
		String resultsInfo = "\"results\": [ ";
		for (ResultObject resultObject : this.results) {
			resultsInfo += resultObject.toJSON() + ",";
		}
		resultsInfo = resultsInfo.substring(0, resultsInfo.length() - 1) + "]";
		return "{" + urlInfo + resultsInfo + "}";
	}

}
