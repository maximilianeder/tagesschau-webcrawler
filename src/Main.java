import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import crawler.Crawler;
import crawler.CrawlerCommand;
import crawler.CrawlerCommandParser;
import file.Read;
import gephi.CSVFactory;
import gephi.FilterFunction;
import result.SiteResult;

public class Main {
	public static void main(String[] args) throws IOException {
		String input;
		String[] crawlerCom;

		if(args.length > 0) {
			input = args[0];
			if (input.contains("FILE > ")) {
				String fileAdress = input.substring(7, input.length());
				File file = new File(fileAdress);
				crawlerCom = Read.read(file);
			} else {
				System.out.println(input);
				throw new RuntimeException("Manual input not supported in Argument");
			}
		}else{
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			input = br.readLine();

			if (input.contains("FILE > ")) {
				String fileAdress = input.substring(7, input.length());
				File file = new File(fileAdress);
				crawlerCom = Read.read(file);
			} else {
				ArrayList<String> commando = new ArrayList<>();
				commando.add(input);
				while (!input.isEmpty()) {
					input = br.readLine();
					System.out.println(input);
					commando.add(input);
				}
				crawlerCom = commando.toArray(new String[commando.size()]);
			}
		}

		CrawlerCommand crawlerCommand = CrawlerCommandParser.parseCrawlerCommand(crawlerCom);

		Crawler c = new Crawler(crawlerCommand);
		CSVFactory csv = new CSVFactory(c.getResults());

		FilterFunction isArticle = (SiteResult sr) -> {
			return "" + !(sr.getResultsByHead("sectionArticle").isEmpty());
		};
		FilterFunction cat = (SiteResult sr) -> {
			String category = "none";
			if(sr.getUrl().contains("tagesschau.de/multimedia/")) {
				category = "multimedia";
			}else if(sr.getUrl().contains("tagesschau.de/inland/")) {
				category = "inland";
			}else if(sr.getUrl().contains("tagesschau.de/ausland/")) {
				category = "ausland";
			}else if(sr.getUrl().contains("tagesschau.de/export/")) {
				category = "export";
			}else if(sr.getUrl().contains("tagesschau.de/wirtschaft/")) {
				category = "wirtschaft";
			}else if(sr.getUrl().contains("tagesschau.de/wahlen/")) {
				category = "wahlen";
			}else if(sr.getUrl().contains("tagesschau.de/mehr/")) {
				category = "mehr";
			}else if(sr.getUrl().contains("tagesschau.de/archiv/")) {
				category = "archiv";
			}else if(sr.getUrl().contains("tagesschau.de/kultur/")) {
				category = "kultur";
			}else if(sr.getUrl().contains("tagesschau.de/sendung/")) {
				category = "sendung";
			}else if(sr.getUrl().contains("tagesschau.de/kommentar/")) {
				category = "kommentar";
			}
			return category;
		};
		csv.filter(isArticle, "isArticle");
		csv.filter(cat, "category");

		csv.writeFiles(new File("/Users/maximilianeder/Desktop/nodes1.csv"), new File("/Users/maximilianeder/Desktop/edges1.csv"));
	}

}
