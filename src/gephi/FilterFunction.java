package gephi;

import result.SiteResult;

public interface FilterFunction {
	String filterSiteResult (SiteResult parameter);
}
