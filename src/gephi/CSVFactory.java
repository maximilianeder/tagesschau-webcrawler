package gephi;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import file.*;
import result.ResultParser;
import result.SiteResult;

public class CSVFactory {
	private List<SiteResult> results;
	private ArrayList<String> nodesTextLines;
	private ArrayList<String> edgesTextLines;
	private HashMap<String, HashSet<String>> linkMap = new HashMap<>();
	private HashMap<String, Integer> idMap = new HashMap<>();

	public CSVFactory(List<SiteResult> results) {
		nodesTextLines = new ArrayList<>();
		edgesTextLines = new ArrayList<>();
		this.results = results;
		baseParse(results);
	}

	public CSVFactory(File jsonInput) {
		nodesTextLines = new ArrayList<>();
		edgesTextLines = new ArrayList<>();

		baseParse(ResultParser.siteResultsFromJSON(jsonInput));
	}

	private void baseParse(List<SiteResult> results) {
		System.out.println("Started CSV Production");
		System.out.println("Started basic parsing");

		/*
		 * Putting all the sites and links into a HashMap
		 */
		for (SiteResult siteResult : results) {
			String siteURL = siteResult.getUrl();
			linkMap.put(siteURL, siteResult.getLinks());
		}

		int idNodes = 0;
		String nodesText = "ID;Label;Crawled";
		nodesTextLines.add(nodesText);

		for (String from : linkMap.keySet()) {
			idMap.put(from, idNodes);
			nodesText = "" + idNodes + ";" + from + ";true";
			nodesTextLines.add(nodesText);
			idNodes++;
		}

		/*
		 * Add Links to Edges csv
		 */

		String edgesText = "Source;Target;Type";
		edgesTextLines.add(edgesText);

		for (String key : linkMap.keySet()) {
			HashSet<String> targets = linkMap.get(key);
			for (String target : targets) {
				if(target.contains(";"))
					continue;
				if(!idMap.containsKey(target))
					idMap.put(target, idNodes); nodesText = "" + idNodes + ";" + target + ";false"; idNodes++; nodesTextLines.add(nodesText);
				edgesText = "" + idMap.get(key) + ";" + idMap.get(target) + ";Directed";
				edgesTextLines.add(edgesText);
			}
		}

		System.out.println("Finished basic parsing");
	}

	public void writeFiles(File outputNodes, File outputEdges) {
		System.out.println("Finished CSV Production");

		String nodesText [] = nodesTextLines.toArray(new String[nodesTextLines.size()]);
		String edgesText [] = edgesTextLines.toArray(new String[edgesTextLines.size()]);

		Write.write(outputNodes, nodesText);
		Write.write(outputEdges, edgesText);

		System.out.println("Wrote CSVs to described locations: \n Nodes: " + outputNodes.getAbsolutePath() + " \n Edges : " + outputEdges.getAbsolutePath());
	}

	public void filter(FilterFunction filterFunc, String columnName) {
		System.out.println("Started filtering for columnName: " + columnName);
		nodesTextLines.set(0, nodesTextLines.get(0) + ";" + columnName);
		for (SiteResult result : results) {
			Integer idC;
			if((idC = idMap.get(result.getUrl())) == null) 
				continue;
			String ret = filterFunc.filterSiteResult(result);
			nodesTextLines.set(idC + 1, nodesTextLines.get(idC + 1) + ";" + ret);
		}
		System.out.println("Finished filtering for columnName: " + columnName);
	}
}
