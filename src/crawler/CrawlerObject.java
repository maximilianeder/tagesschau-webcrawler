package crawler;
import java.util.ArrayList;

public class CrawlerObject {
	private String head;
	private Identification headType;
	private ArrayList<CrawlerDetail> details;
	private boolean addLinks;
	private boolean onlyOnce;

	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public Identification getHeadType() {
		return headType;
	}

	public void setHeadType(Identification headType) {
		this.headType = headType;
	}

	public ArrayList<CrawlerDetail> getDetails() {
		return details;
	}

	public void setDetails(ArrayList<CrawlerDetail> details) {
		this.details = details;
	}

	public boolean isAddLinks() {
		return addLinks;
	}

	public void setAddLinks(boolean addLinks) {
		this.addLinks = addLinks;
	}

	public boolean isOnlyOnce() {
		return onlyOnce;
	}

	public void setOnlyOnce(boolean onlyOnce) {
		this.onlyOnce = onlyOnce;
	}

	public CrawlerObject(String head, Identification headType, ArrayList<CrawlerDetail> details, boolean addLinks,
			boolean onlyOnce) {
		super();
		this.head = head;
		this.headType = headType;
		this.details = details;
		this.addLinks = addLinks;
		this.onlyOnce = onlyOnce;
	}
}
