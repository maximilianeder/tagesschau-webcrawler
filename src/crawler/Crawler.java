package crawler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.jsoup.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import file.Write;
import gephi.CSVFactory;
import result.Result;
import result.ResultObject;
import result.SiteResult;

public class Crawler {
	private int sFlag = -1;
	private boolean pf = false;
	private boolean ps = false;
	private boolean initRun = true;
	private boolean uFlag = false;
	private long timeLimit = Long.MAX_VALUE;
	private boolean stay = false;
	private int mtFlag = 1;

	private Object sLock = new Object();
	private Object crawledLock = new Object();
	private Object toCrawlLock = new Object();
	private Object crawlingLock = new Object();
	private Object resultsLock = new Object();

	private CrawlerCommand crawlerCommand;

	private ArrayList<String> sitesToCrawl = new ArrayList<>();
	private ArrayList<String> sitesCrawled = new ArrayList<>();
	private ArrayList<String> sitesCrawling = new ArrayList<>();
	private ArrayList<SiteResult> siteResults = new ArrayList<>();

	class CrawlerThread implements Runnable {

		@Override
		public void run() {
			CrawlerCommand command = crawlerCommand;
			boolean breakPoint = false;

			while (true) {
				boolean temp = true;
				synchronized (toCrawlLock) {
					temp = sitesToCrawl.isEmpty();
				}
				if (temp) {
					if (breakPoint) {
						System.out.println(
								"Thread " + Thread.currentThread().getId() + " stopped - No sites left to crawl");
						break;
					}
					breakPoint = true;
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					continue;
				}
				breakPoint = false;

				if (System.nanoTime() >= timeLimit) {
					System.out.println("Thread " + Thread.currentThread().getId() + " stopped - TimeLimit reached");
					break;
				}

				synchronized (sLock) {
					if (sFlag <= 0) {
						System.out.println("Thread " + Thread.currentThread().getId() + " stopped - SiteLimit reached");
						break;
					}
					sFlag--;
				}

				ArrayList<ResultObject> results = new ArrayList<>();
				String currentURL;
				synchronized (toCrawlLock) {
					currentURL = sitesToCrawl.remove(0);
					synchronized (crawlingLock) {
						sitesCrawling.add(currentURL);
					}
				}

				if (ps) {
					System.out.println("Starting analysis of " + currentURL);
				}
				Connection connection;
				try {
					connection = Jsoup.connect(currentURL);
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
					continue;
				}
				Document document;
				try {
					document = connection.get();
					for (CrawlerObject co : command.getObjects()) {
						if (co.isOnlyOnce() && !initRun) {
							continue;
						}

						Elements elems;
						if (co.getHeadType() == Identification.clas) {
							elems = document.getElementsByClass(co.getHead());
						} else {
							elems = document.getElementsByTag(co.getHead());
						}

						for (Element element : elems) {
							HashMap<String, ArrayList<String>> map = new HashMap<>();
							ArrayList<String> linksList = null;

							/*
							 * Go through all the Crawler-Details
							 */
							for (CrawlerDetail detail : co.getDetails()) {
								ArrayList<String> list = new ArrayList<>();

								Elements targets;
								if (detail.getId() == Identification.tag) {
									targets = element.getElementsByTag(detail.getName());
								} else {
									targets = element.getElementsByClass(detail.getName());
								}

								for (Element target : targets) {
									list.add(target.text());
								}

								map.put(detail.getName(), list);
							}

							/*
							 * If activated, scan for links in the Crawler-Object
							 */
							if (co.isAddLinks()) {
								linksList = new ArrayList<>();
								Elements links = element.getElementsByTag("a");
								for (Element link : links) {
									String hrefs = link.attr("href");
									String linksString;
									// Check, if Link is correct or just a partial link
									if (hrefs.contains("http://") || hrefs.contains("https://")) {
										linksString = hrefs;
									} else {
										if (hrefs.contains("//") && hrefs.substring(0, 2).equals("//")) {
											linksString = "https:" + hrefs;
										} else {
											if (command.getMainSiteUrl()
													.charAt(command.getMainSiteUrl().length() - 1) == '/') {
												linksString = command.getMainSiteUrl()
														+ hrefs.substring(1, hrefs.length());
											} else {
												if (!hrefs.contains("mailto:")) {
													linksString = command.getMainSiteUrl() + hrefs;
												} else {
													continue;
												}
											}
										}
									}

									if (!linksString.contains(".html")) {
										if (linksString.charAt(linksString.length() - 1) == '/') {
											linksString += "index.html";
										}
									}

									if (!linksList.contains(linksString))
										linksList.add(linksString);

									synchronized (toCrawlLock) {
										synchronized (crawledLock) {
											synchronized (crawlingLock) {
												if (!sitesToCrawl.contains(linksString)
														&& !sitesCrawled.contains(linksString)
														&& !sitesCrawling.contains(linksString)) {
													if (!stay || linksString.contains(command.getMainSiteUrl())) {
														sitesToCrawl.add(linksString);
														if (pf) {
															System.out.println("Found link to: " + linksString);
														}
													}
												}
											}
										}
									}
								}

							}

							ResultObject r = new ResultObject(currentURL, co.getHead(), map, linksList);
							if (r.isRelevant() || uFlag) {
								results.add(r);
							}
						}

					}
					if (initRun) {
						initRun = false;
					}

					synchronized (crawledLock) {
						sitesCrawled.add(currentURL);
						synchronized (crawlingLock) {
							sitesCrawling.remove(currentURL);
						}
					}
					if (ps) {
						System.out.println("Finished analysis of " + currentURL);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

				synchronized (resultsLock) {
					siteResults.add(new SiteResult(currentURL, results));
				}
			}
		}

	}

	/**
	 * Creates a Crawler that is specified by the CrawlerCommand given to it. It
	 * then starts to crawl and scrape the sites as specified and prints usefull
	 * information out on the console
	 * 
	 * @param command
	 *            CrawlerCommand, that specifies the sites to crawl and the
	 *            information to scrape
	 */
	public Crawler(CrawlerCommand command) {
		crawlerCommand = command;

		String optionsString = command.getOptions();
		String[] options = optionsString.split("-");
		for (String string : options) {
			if (string.length() > 0 && string.charAt(0) == 'p') {
				try {
					String s = string.substring(2, string.length());
					if (s.contains("f")) {
						pf = true;
						System.out.println("Enabled 'print found' print");
					}
					if (s.contains("s")) {
						ps = true;
						System.out.println("Enabled 'analysis state' print");
					}
				} catch (NumberFormatException e) {
					throw new RuntimeException("The s-Flag only takes integers as parameter");
				}
			} else if (string.length() > 0 && string.charAt(0) == 'u') {
				uFlag = true;
			} else if (string.length() > 3 && string.substring(0, 4).equals("stay")) {
				stay = true;
			} else if (string.length() > 0 && string.charAt(0) == 's') {
				try {
					sFlag = Integer.parseInt(clearUp(string.substring(2, string.length())));
					System.out.println("Analyzing the first " + sFlag + " Sites");
				} catch (NumberFormatException e) {
					throw new RuntimeException("The s-Flag only takes integers as parameter");
				}
			} else if (string.length() > 1 && string.substring(0, 2).equals("mt")) {
				try {
					mtFlag = Integer.parseInt(clearUp(string.substring(2, string.length())));
					if (mtFlag < 1)
						throw new NumberFormatException();
					System.out.println("Activated Multithreading: Working with " + mtFlag + " Threads.");
				} catch (NumberFormatException e) {
					throw new RuntimeException("The mt-Flag only takes integers > 1 as parameter");
				}
			} else if (string.length() > 0 && string.charAt(0) == 't') {
				try {
					long time = Long.parseLong(clearUp(string.substring(2, string.length())));
					System.out.println("Analyzing for " + time + " seconds");
					timeLimit = System.nanoTime() + time * 1_000_000_000;
				} catch (NumberFormatException e) {
					throw new RuntimeException("The t-Flag only takes integers as parameter");
				}
			}
		}

		sitesToCrawl.add(command.getUrl());

		Thread[] threads = new Thread[mtFlag];
		for (int i = 0; i < mtFlag; i++) {
			threads[i] = new Thread(new CrawlerThread());
			threads[i].start();
		}

		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		String reason = "";
		if (System.nanoTime() > timeLimit) {
			reason = " - reached Time Limit";
		} else if (sFlag == 0) {
			reason = " - reached Site Limit";
		} else if (sitesToCrawl.isEmpty()) {
			reason = " - crawled all sites";
		} else {
			reason = " - unknown reason";
		}

		System.out.println("Ended analysis of " + command.getMainSiteUrl() + reason);

		generateOutput();
	}

	private String clearUp(String string) {
		String result = "";
		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) == ' ' || string.charAt(i) == '\t' || string.charAt(i) == '\n') {
				// Drop character
			} else {
				result += string.charAt(i);
			}
		}
		return result;
	}

	/**
	 * Returns a list of all SiteURLs, that have been crawled
	 * 
	 * @return All crawled sites
	 */
	public ArrayList<String> getSitesCrawled() {
		return sitesCrawled;
	}

	/**
	 * Returns a list of all the SiteResults of the sites that have been crawled
	 * 
	 * @return List of SiteResults
	 */
	public ArrayList<SiteResult> getResults() {
		return siteResults;
	}

	/**
	 * Generates the output specified in the CrawlerCommand
	 */
	public void generateOutput() {
		String[] outputs = crawlerCommand.getOutput().split(";");

		for (String string : outputs) {
			String[] output = string.split(" ");

			switch (output[0]) {
			case "file":
				if (output.length < 2) {
					System.out.flush();
					System.err.println("No file specified: " + string + " - Stopped task");
					try {
						TimeUnit.SECONDS.sleep(3l);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					// System.out.println(Result.toJSON(getResults()));
				} else {
					file(output[1]);
				}
				break;
			case "print":
				print();
				break;
			case "database":
				// TODO implement database output
				break;
			case "csv":
				if (output.length < 3) {
					System.out.flush();
					System.err.println("No output for nodes.csv and edges.csv: " + string + " - Stopped task");
					try {
						TimeUnit.SECONDS.sleep(3l);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else {
					new CSVFactory(siteResults).writeFiles(new File(output[1]), new File(output[2]));
					System.out.println("Wrote nodes.csv into file " + output[1]);
					System.out.println("Wrote edges.csv into file " + output[2]);
				}
				break;
			case "none":
				break;
			default:
				break;
			}
		}
	}

	/**
	 * Prints the result to the console
	 */
	public void print() {
		for (SiteResult result : getResults()) {
			result.print();
		}
	}

	/**
	 * Writes the result to the file specified by input
	 * 
	 * @param file
	 *            destination file path
	 */
	public void file(String file) {
		Write.write(file, Result.toJSON(getResults()));
		System.out.println("Wrote JSON into file " + file);
	}

	/**
	 * Writes the result to the file specified by input
	 * 
	 * @param file
	 *            destination file
	 */
	public void file(File file) {
		Write.write(file, Result.toJSON(getResults()));
		System.out.println("Wrote JSON into file " + file);
	}
}
