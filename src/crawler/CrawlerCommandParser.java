package crawler;
import java.util.ArrayList;

import org.json.*;

/**
 * @author Maximilian Eder
 *
 */
public class CrawlerCommandParser {

	/**
	 * Parses a String[] in JSON format and creates a new {@link CrawlerCommand}
	 * @param crawlerCommand String Array read out of JSON-file or from console
	 * @return {@link CrawlerCommand} Object used in {@link Crawler}
	 */
	public static CrawlerCommand parseCrawlerCommand(String[] crawlerCommand) {
		String json = arrayToString(crawlerCommand);
		JSONObject headObject = new JSONObject(json);

		String url = headObject.getString("site");
		String mainUrl = headObject.getString("mainsite");
		String options = headObject.getString("options");
		String output = headObject.getString("output");

		JSONArray objectArray = headObject.getJSONArray("objects");
		ArrayList<JSONObject> list = new ArrayList<>();

		for (int i = 0; i < objectArray.length(); i++) {
			list.add(objectArray.getJSONObject(i));
		}

		ArrayList<CrawlerObject> crawlerObjects = new ArrayList<>();

		for (int i = 0; i < list.size(); i++) {
			JSONObject object = list.get(i);

			String head = object.getString("head");
			Identification headType;
			String[] array = head.split(" ", 2);
			if(array[0].equals("<tag>")) {
				head = array[1];
				headType = Identification.tag;
			}else if(array[0].equals("<class>")) {
				head = array[1];
				headType = Identification.clas;
			}else{
				throw new RuntimeException("Unknown CrawlerDetailID");
			}

			JSONArray detailArray = object.getJSONArray("details");
			boolean addLinks = object.getBoolean("addLinks");
			boolean onlyOnce = object.getBoolean("onlyOnce");

			ArrayList<String> details = new ArrayList<>();
			for (int j = 0; j < detailArray.length(); j++) {
				details.add(detailArray.getString(j));
			}

			ArrayList<CrawlerDetail> crawlerDetails = new ArrayList<>();

			for (String string : details) {
				array = string.split(" ", 2);
				if(array[0].equals("<tag>")) {
					crawlerDetails.add(new CrawlerDetail(array[1], Identification.tag));
				}else if(array[0].equals("<class>")) {
					crawlerDetails.add(new CrawlerDetail(array[1], Identification.clas));
				}else{
					throw new RuntimeException("Unknown CrawlerDetailID");
				}
			}

			crawlerObjects.add(new CrawlerObject(head, headType, crawlerDetails, addLinks, onlyOnce));
		}

		return new CrawlerCommand(url, mainUrl, output, options, crawlerObjects);
	}

	private static String arrayToString(String[] crawlerCommand) {
		String collection = "";
		for (String string : crawlerCommand) {
			collection += clearUp(string);
		}
		return collection;
	}

	private static String clearUp(String string) {
		String collection = "";
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			if(c != '\t') {
				if(c == '\n') {
					c = ' ';
				}
				collection += c;
			}
		}
		return collection;
	}
}
