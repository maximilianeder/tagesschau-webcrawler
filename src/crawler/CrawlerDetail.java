package crawler;

public class CrawlerDetail {
	private String name;
	private Identification id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Identification getId() {
		return id;
	}

	public void setId(Identification id) {
		this.id = id;
	}

	public CrawlerDetail(String name, Identification id) {
		super();
		this.name = name;
		this.id = id;
	}
}
