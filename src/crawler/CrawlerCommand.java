package crawler;
import java.util.ArrayList;

/**
 * @author Maximilian Eder
 *
 */
public class CrawlerCommand {
	private String url;
	private String mainSiteUrl;
	private String output;
	private String options;

	/**
	 * CralerCommand used by {@link Crawler} to specify sites to crawl and info to scrape; gets created by {@link CrawlerCommandParser}
	 *
	 * @param url Site URL
	 * @param mainSiteUrl Main Site URL
	 * @param output Output method (print; file <filename>)
	 * @param options Options String
	 * @param objects List of {@link CrawlerObject}s that shall be scraped
	 */
	public CrawlerCommand(String url, String mainSiteUrl, String output, String options,
			ArrayList<CrawlerObject> objects) {
		super();
		this.url = url;
		this.mainSiteUrl = mainSiteUrl;
		this.output = output;
		this.options = options;
		this.objects = objects;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	private ArrayList<CrawlerObject> objects;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMainSiteUrl() {
		return mainSiteUrl;
	}

	public void setMainSiteUrl(String mainSiteUrl) {
		this.mainSiteUrl = mainSiteUrl;
	}

	public ArrayList<CrawlerObject> getObjects() {
		return objects;
	}

	public void setObjects(ArrayList<CrawlerObject> objects) {
		this.objects = objects;
	}

}
