# Tagesschau WebCrawler

## Overview

This project is a showcase project for a webcrawler that I have written in 2018. It dynamically crawles throught the Tagesschau.de (German prime-time news) repository and collects data from news stories featured on the website. Meanwhile it searches for links and references to older stories and adds them as context, dynamically branching out the reach of the scraper back in time. The data that is stored about these articles can be analyzed in a plethora of ways, but this project focussed on a graph-bbased view of the links between the articles that are on Tagesschau.de and how those links can be visualized using the graph visualization tool "Gephi" (gephi.github.io).

**This project has been transferred here as a showcase project from its previous private repository. It has not been updated since 2018 and is not actively maintained anymore. Usage of this code happens on your own risk and responsibility. Do make sure that using this crawler on Tagesschau.de is not prohibited by the websites terms of use before using it. I will not be responsible for any problems that arise from using this software.**

## Installation

This project uses the distributions of the Java JSON package version 2018-08-13 and the JSoup package version 1.11.3. Put both jars in the project folder and add them to the java runtime path.